package com.formacionbdi.microservicios.app.usuarios.controllers;

import com.formacionbdi.microservicios.app.usuarios.models.entity.Alumno;
import com.formacionbdi.microservicios.app.usuarios.services.AlumnoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.Optional;

/*
@RestController es una anotación en Spring que se utiliza para crear
controladores de RESTful en aplicaciones web. Al agregar la anotación @RestController
a una clase de controlador en Spring, se indica que esta clase manejará solicitudes HTTP
 y devolverá respuestas HTTP en formato JSON, XML o cualquier otro formato. Esto facilita
la creación de aplicaciones web que siguen la arquitectura RESTful y permite la comunicación
entre diferentes sistemas de software a través de una API web.
* */
@RestController
public class AlumnoController {

    //Siempre implementar tipos genericos
    @Autowired
    private AlumnoService alumnoService;

    //Listar
    /*
    @GetMapping es una anotación en Spring que se utiliza para mapear una solicitud HTTP GET a un método de controlador específico.
 * */
    @GetMapping
    public ResponseEntity<?> listar(){
        return ResponseEntity.ok().body(alumnoService.findAll());
        /*
       En Spring, ResponseEntity.ok().body() se utiliza para crear una
       respuesta HTTP con estado 200 (OK) y un cuerpo de respuesta personalizado
        que puede ser cualquier objeto Java. Es una forma de enviar datos de respuesta
         a un cliente que realiza una solicitud HTTP a un servicio web de Spring.
         El método "ok()" indica que la solicitud se procesó correctamente y el método "body()"
         establece el cuerpo de la respuesta con el objeto proporcionado. En resumen,
          esta construcción se utiliza para enviar una respuesta exitosa con un objeto personalizado
           en un servicio web de Spring.
        * */
    }

    //Ver
    @GetMapping("/{id}") //Indicamos que por la ruta recibe un valor
    public ResponseEntity<?> ver(@PathVariable Long id){
        //1.- Vamos a buscar el Alumno en la base de datos
        Optional<Alumno> o= alumnoService.findById(id);

        //2.- Verificamos que lo halla encontrado en la base de datos
        if (o.isPresent()){ return ResponseEntity.ok().body(o.get()); } //Si lo encuentra retorna el objeto
        return ResponseEntity.notFound().build(); //Si no lo encuentra manda un NotFound
        /*
        * código de estado 404 (Not Found), que indica que el recurso
        * solicitado no fue encontrado en el servidor.
         * */
        /*
        * @PäthVariable: Esto permite que los valores proporcionados en
        * la URL se pasen como argumentos en un método, lo que permite
        * a los desarrolladores construir aplicaciones web dinámicas y escalables en Spring.
         * */
    }


}
