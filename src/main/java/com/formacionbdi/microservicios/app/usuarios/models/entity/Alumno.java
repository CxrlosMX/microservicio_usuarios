package com.formacionbdi.microservicios.app.usuarios.models.entity;


import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import java.util.Date;

/**
 * Tabla BBDD Usuarios
 */
@Entity
@Table(name = "alumnos")
@Getter
@Setter
public class Alumno {
    /**
     * Indicamos que es una llave
     * E informamos que se debe de generar de 1 en 1 por cada registro
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nombre;

    private String apellido;

    private String email;

    /**
     * Indicamos el nombre de la columna dentro de la base de datos
     */
    @Column(name = "create_at")
    @Temporal(TemporalType.TIMESTAMP)
    private Date createAt;

    /**
     * Asignamos el valor de
     */
    @PrePersist
    public void prePersist(){
        this.createAt=new Date();
    }
}
