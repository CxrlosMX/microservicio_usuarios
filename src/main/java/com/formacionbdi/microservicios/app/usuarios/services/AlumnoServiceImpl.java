package com.formacionbdi.microservicios.app.usuarios.services;

import com.formacionbdi.microservicios.app.usuarios.models.entity.Alumno;
import com.formacionbdi.microservicios.app.usuarios.models.repository.AlumnoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Indicamos que es un componente, como contenedor de Spring
 */
@Service
public class AlumnoServiceImpl implements AlumnoService{

    @Autowired
    private AlumnoRepository alumnoRepository;

    @Override
    @Transactional(readOnly = true)  //Para indicar que solo es de consulta dentro de la base de datos
    public Iterable<Alumno> findAll() {
        return alumnoRepository.findAll();
    }

    /**
     *
     * @param id
     * @return el alumno encontrado
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Alumno> findById(Long id) {
        return alumnoRepository.findById(id);
    }

    /**
     *
     * @param alumno
     * @return el alumno guardado en la base de datos
     */
    @Override
    @Transactional //Para indicar que permite la creación y editar dentro de la tabla de la base de datos
    public Alumno save(Alumno alumno) {
        return alumnoRepository.save(alumno);
    }

    /**
     *
      * @param id
     */
    @Override
    @Transactional
    public void deleteById(Long id) {
     alumnoRepository.deleteById(id);
    }
}
